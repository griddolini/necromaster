extends KinematicBody2D

const Powerline = preload("res://scenes/PowerLine.gd")
const Undead = preload("res://Undead/Undead.gd")

export var move_speed := 60
export var whip_damage := 2
export var harvest_cooldown := 5.0
export var teleport_cooldown := 4.0
var input_vector := Vector2.ZERO
var movement := Vector2.ZERO
var calculated_move := Vector2.ZERO

var mouse_pos := Vector2.ZERO
var angle_to_mouse := 0.0
var targeted_by_count := 0
var power_already_drawn = false
var connected := false

var can_harvest = true
var can_teleport = true

func _ready():
	globals.player = self
	pass
	
func _physics_process(_delta):
	input_vector = Vector2.ZERO
	mouse_pos = get_global_mouse_position()
	angle_to_mouse = self.global_position.angle_to_point(mouse_pos) + PI
	
	# Get movement input
	if $CharSprite/AnimationPlayer.current_animation != "Teleport" and $CharSprite/AnimationPlayer.current_animation != "LifeHarvest":
		if Input.is_action_pressed("move_left"):
			input_vector.x -= 1
		if Input.is_action_pressed("move_right"):
			input_vector.x += 1
		if Input.is_action_pressed("move_up"):
			input_vector.y -= 1
		if Input.is_action_pressed("move_down"):
			input_vector.y += 1
	
	if $CharSprite/AnimationPlayer.current_animation == "Idle" or $CharSprite/AnimationPlayer.current_animation == "Walking":
		if Input.is_action_pressed("left_click"):
			$CharSprite/AnimationPlayer.play("WhipAttack", -1, 1.0)
		elif Input.is_action_pressed("right_click"):
			$CharSprite/AnimationPlayer.play("PowerWhip", -1, 1.0)
		elif Input.is_action_just_pressed("shift") and can_teleport:
			can_teleport = false
			$TeleportCooldown.start(teleport_cooldown)
			$CharSprite/AnimationPlayer.play("Teleport", -1, 0.8)
			self.global_position = get_global_mouse_position()
			effects.create_hiteffect(self.global_position, 2, self)
		elif Input.is_action_just_pressed("space") and can_harvest:
			can_harvest = false
			$HarvestCooldown.start(harvest_cooldown)
			$UsePowerCooldown.start(1)
			$CharSprite/AnimationPlayer.play("LifeHarvest", -1, 0.8)
			effects.create_hiteffect(self.global_position, 3, self)
			globals.harvest_life()
		else:
			if input_vector.length() > 0:
				$CharSprite/AnimationPlayer.play("Walking")
			else:
				$CharSprite/AnimationPlayer.play("Idle")
				
	
	# Rotate based on mouse position
	if mouse_pos.x < self.global_position.x:
		$CharSprite.set_flip_h(true)
		$CharSprite/WhipSprite.set_flip_v(true)
	else:
		$CharSprite.set_flip_h(false)
		$CharSprite/WhipSprite.set_flip_v(false)
		
	# Rotate our whip
	$CharSprite/WhipSprite.set_rotation(angle_to_mouse)
	$CharSprite/WhipCollider.set_rotation(angle_to_mouse)
	
	# Apply movement
	input_vector = input_vector.normalized()
	movement = lerp(movement, input_vector * move_speed, 0.2)
	movement = move_and_slide(movement)
	
	if not can_harvest:
		globals.ui.update_harvest_cd(int(($HarvestCooldown.time_left/harvest_cooldown) * 100))
	if not can_teleport:
		globals.ui.update_teleport_cd(int(($TeleportCooldown.time_left/teleport_cooldown) * 100))
	pass

func do_whip(power:float):
	var whippers = $CharSprite/WhipCollider.get_overlapping_bodies()
	for whippee in whippers:
		if (whippee as KinematicBody2D).is_in_group("human"):
			globals.player_power += 1
			globals.score_power += 1
			if power > 100:
				globals.player_power += 1
				globals.score_power += 1
			globals.ui.update_power()
			whippee.take_whip(angle_to_mouse, whip_damage, power)
		elif (whippee as KinematicBody2D).is_in_group("undead"):
			whippee.take_whip(angle_to_mouse, power)
	pass

func _on_LinkRange_body_entered(body : KinematicBody2D):
	if body:
		if body.is_in_group("undead") and not connected:
			body.get_node("Ring").set_visible(true)
			globals.draw_powerline(self, body as KinematicBody2D)
		pass

func _on_LinkRange_body_exited(body : KinematicBody2D):
	if body:
		if body.is_in_group("undead"):
			if not body.spawned_line and not body.has_line:
				body.get_node("Ring").set_visible(false)
			for i in range(globals.power_connections.size() - 1, -1, -1):
				if globals.power_connections[i].ending_unit == body and globals.power_connections[i].starting_unit == self:
					connected = false
					globals.power_connections[i].queue_free()
					globals.power_connections.remove(i)
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	$CharSprite/AnimationPlayer.play("Idle", -1, 1.0)
	pass
	
func used_power_timer():
	power_already_drawn = false


func _on_HarvestCooldown_timeout():
	can_harvest = true
	globals.ui.harvest_ready()
	pass


func _on_TeleportCooldown_timeout():
	can_teleport = true
	globals.ui.teleport_ready()
	pass
