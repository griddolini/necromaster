extends CanvasLayer

func _ready():
	globals.ui = self

func update_power():
	$RootControl/PowerBar.set_value(globals.player_power)
	if globals.player_power >= 100:
		globals.player_power = 100
		$RootControl/GOPOWER.set_visible(true)
	else:
		$RootControl/GOPOWER.set_visible(false)
	pass

func update_harvest_cd(value : int):
	$RootControl/Ability1/Ability1CD.set_visible(true)
	$RootControl/Ability1/Cooldown1.set_value(100 - value)
	pass

func harvest_ready():
	$RootControl/Ability1/Ability1CD.set_visible(false)
	pass
	
func update_teleport_cd(value : int):
	$RootControl/Ability2/Ability2CD.set_visible(true)
	$RootControl/Ability2/Cooldown2.set_value(100 - value)
	pass

func teleport_ready():
	$RootControl/Ability2/Ability2CD.set_visible(false)
	pass

func update_health():
	$RootControl/CurrentHealth.text = "LIFE " + str(globals.player_health)
	pass
	
func update_tether_count():
	$PassiveChainCount.text = str(globals.get_harvest_count())

func show_game_over():
	$GameOver.set_visible(true)
	$GameOver/GameOver2.text = "power score - " + str(globals.score_power)
	$GameOver/GameOver2.text += "\nkill score - " + str(globals.score_kills)
	$GameOver/GameOver2.text += "\ntime score - " + str(globals.score_time)

func show_harvest_combo(count):
	$ActiveChainCount.text = str(count)
	$ActiveChainCount.set_visible(true)
	$HarvestScoreTimer.start(3)
	
func _on_HarvestScoreTimer_timeout():
	$ActiveChainCount.set_visible(false)
	pass

func _on_UpdateScores_timeout():
	$ScoreBoard.text = str(globals.score_power) + " total power\n"
	$ScoreBoard.text += str(globals.score_kills) + " total kills\n"
	globals.score_time += 1
	$ScoreBoard.text += str(globals.score_time) + " time survived"
	pass
