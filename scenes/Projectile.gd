extends Node2D

var speed : float = 3
var damage = 4.0
var lifetime := 0.2
var texture : Texture
var direction = Vector2.ZERO
var angle = 0.0
var team_target = ""

func _ready():
	$ImpactRay.cast_to.x = speed
	$ImpactRay.rotate(angle)
	$Sprite.texture = texture
	$Sprite.rotate(angle)
	$Timer.start(lifetime)
	pass

func _physics_process(_delta):
	self.global_position += $ImpactRay.cast_to.rotated($ImpactRay.rotation)
	if $ImpactRay.is_colliding() and not $ImpactRay.get_collider().get_parent().is_in_group("player"):
		do_impact($ImpactRay.get_collider())
	pass

func do_impact(collider : KinematicBody2D):
	if collider.is_in_group(team_target):
		collider.take_damage(damage)
		queue_free()
	pass

func _on_Timer_timeout():
	queue_free()
