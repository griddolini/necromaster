extends RigidBody2D

export(PackedScene) var human_1
export(PackedScene) var human_2
export(PackedScene) var human_3

export var spawn_speed = 12
export var upgrade_speed = 20

func _ready():
	self.apply_impulse(Vector2.ZERO, (Vector2.RIGHT*50).rotated(rand_range(-PI, PI)))
	pass

func timer_timeout():
	var chance = rand_range(0,100)
	var new_spawn = null
	if chance < 50:
		new_spawn = human_1.instance()
	elif chance < 80:
		new_spawn = human_2.instance()
	else:
		new_spawn = human_3.instance()
		
	globals.player.get_parent().add_child(new_spawn)
	new_spawn.global_position = self.global_position
	$Timer.start(spawn_speed)

func _on_Upgrade_timeout():
	if spawn_speed > 5:
		spawn_speed -= 1
	$Upgrade.start(upgrade_speed)
	pass
