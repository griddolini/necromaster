extends Area2D

var starting_unit = null
var ending_unit = null

var start_pos := Vector2.ZERO
var end_pos := Vector2.ZERO
var shape = null
var targeted_by_count = 1000

func _ready():
	shape = RectangleShape2D.new()
	$CollisionShape2D.set_shape(shape)
	pass

func _physics_process(_delta):
	start_pos = starting_unit.global_position
	end_pos = ending_unit.global_position
	self.global_position = (start_pos + end_pos) / 2
	
	$CollisionShape2D.set_rotation(get_angle_to(end_pos))
	shape.set_extents(Vector2((start_pos - end_pos).length()/2, 4))

	$Sprite.set_rotation(get_angle_to(end_pos))
	$Sprite.set_region_rect(Rect2(0.0, 0.0, (start_pos - end_pos).length()-8, 4))
	pass

func _on_PowerLine_body_entered(body : KinematicBody2D):
	if body.is_in_group("human"):
		globals.power_targets.append(body)
		globals.ui.update_tether_count()
		body.get_node("Ring").set_visible(true)
	pass

func _on_PowerLine_body_exited(body : KinematicBody2D):
	if body.is_in_group("human"):
		globals.power_targets.remove(globals.power_targets.find(body))
		# Check for dupes
		if globals.power_targets.find(body) == -1:
			body.get_node("Ring").set_visible(false)
		globals.ui.update_tether_count()
	pass
