extends KinematicBody2D

var projectile_scene = preload("res://scenes/Projectile.tscn")

export(PackedScene) var body_chunks
export var max_hp := 10
var current_hp := 10
export var stun_duration := 5
export var move_speed = 20
export var attack_duration = 0.1
export var attack_damage = 3
export var attack_speed = 1
export var attack_sprite : Texture = preload("res://scenes/slash1.png")
export var attack_range = 20
export var attack_animation_speed = 0.5

var movement := Vector2.ZERO
var force_vector := Vector2.ZERO

var target_direction := PI
var move_target = null
var move_target_index = -1
var possible_targets = []
var targeted_by_count = 0

var in_attack_range = false
var stunned := false
var was_harvested := false
var spawned_line := false
var has_line := false
var greenfire = preload("res://effects/GreenFire.tscn")

func _ready():
	current_hp = max_hp
	var entry = greenfire.instance()
	globals.player.get_parent().call_deferred("add_child", entry)
	entry.global_position = self.global_position
	
	globals.undead_count += 1
	var nearby_undead = globals.player.get_parent().get_children()
	if nearby_undead.size() > 0:
		var closest_dist = 0
		var closest_unit = null
		for close in nearby_undead:
			if close.is_in_group("undead") and close != self:
				if not close.has_line:
					closest_unit = close
		if closest_unit != null:
			closest_unit.get_node("Ring").set_visible(true)
			spawned_line = true
			self.get_node("Ring").set_visible(true)
			closest_unit.has_line = true
			globals.draw_powerline(self, closest_unit)
	pass

func _physics_process(delta):
	if not is_instance_valid(move_target):
		if move_target_index != -1 and possible_targets.size() > move_target_index:
			possible_targets.remove(move_target_index)
		move_target_index = -1
		recalculate_target()
	if not stunned and move_target:
		target_direction = self.global_position.angle_to_point(move_target.global_position) + PI
		set_facing_direction()
		# Check to see if we are in attack range
		if self.global_position.distance_to(move_target.global_position) < attack_range:
			in_attack_range = true
		else:
			in_attack_range = false
			movement = lerp(movement, Vector2.RIGHT.rotated(target_direction) * move_speed, 0.5)
			
	# Slow external forces
	force_vector = lerp(force_vector, Vector2.ZERO, 0.05)
	# Apply external forces and move
	movement = lerp(movement, force_vector, 0.5)
	movement = move_and_slide(movement)
	pass

func attack():
	var arrow = projectile_scene.instance()
	arrow.set_global_position(self.global_position)
	arrow.angle = target_direction
	arrow.damage = attack_damage
	arrow.lifetime = attack_duration
	arrow.texture = attack_sprite
	arrow.team_target = "human"
	globals.player.get_parent().add_child(arrow)
	pass

func take_whip(pushdir:float, power:float):
	# Apply push, apply stun effect
	force_vector = Vector2.RIGHT.rotated(pushdir) * power
	get_stunned()
	pass
	
func take_damage(damage:int):
	effects.create_hiteffect(self.global_position, 1)
	($CharSprite/AnimationPlayer as AnimationPlayer).stop(true)
	($CharSprite/AnimationPlayer as AnimationPlayer).play("Hurt")
	current_hp -= damage
	$HP.set_value(current_hp * int(100 / max_hp))
	if current_hp <= 0:
		die()

func get_stunned():
	if self.has_node("StunnedEffect"):
		self.get_node("StunnedEffect").queue_free()
	effects.create_stuneffect(self.global_position, stun_duration,  self)
	$StunTimer.start(stun_duration)
	stunned = true

func _on_StunTimer_timeout():
	if stunned:
		stunned = false
		$CharSprite/AnimationPlayer.play("Idle")
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	if not stunned and in_attack_range:
		if anim_name == "Attack" or move_target == globals.player:
			$CharSprite/AnimationPlayer.play("Idle", -1, 1.0)
		else:
			$CharSprite/AnimationPlayer.play("Attack", -1, attack_animation_speed)
	elif not stunned:
		$CharSprite/AnimationPlayer.play("Walking", -1, 1.0)
	pass

func die():
	var entry = body_chunks.instance()
	globals.player.get_parent().add_child(entry)
	entry.global_position = self.global_position
	
	globals.undead_count -= 1
	if globals.undead_count <= 0:
		globals.ui.show_game_over()
	for i in range(globals.power_connections.size() - 1, -1, -1):
		if globals.power_connections[i].ending_unit == self or globals.power_connections[i].starting_unit == self:
			globals.power_connections[i].ending_unit.spawned_line = false
			globals.power_connections[i].ending_unit.has_line = false
			globals.power_connections[i].queue_free()
			globals.power_connections.remove(i)
	queue_free()
	pass
	
func _on_HarvestTimer_timeout():
	was_harvested = false
	pass

func _on_Perception_body_entered(body : KinematicBody2D):
	if body:
		if body.is_in_group("human"):
			possible_targets.append(body)
	pass 

func _on_Perception_body_exited(body : KinematicBody2D):
	if body:
		if body.is_in_group("human"):
			var body_index = possible_targets.find(body)
			if body_index != -1:
				possible_targets.remove(possible_targets.find(body))
				if move_target_index == body_index:
					move_target_index = -1
			if body == move_target:
				body.targeted_by_count -= 1
				recalculate_target()
	pass

func recalculate_target():
	# Of everything we can see, find the best target
	if possible_targets.size() > 0:
		if move_target == null or not is_instance_valid(move_target):
			change_target_to(globals.player)
		if is_instance_valid(move_target):
			for target in possible_targets:
				if target.targeted_by_count < move_target.targeted_by_count - 1:
					move_target_index = possible_targets.find(target)
					change_target_to(target)
				elif target.targeted_by_count == move_target.targeted_by_count - 1:
					if self.global_position.distance_to(move_target.global_position) > self.global_position.distance_to(target.global_position):
						move_target_index = possible_targets.find(target)
						change_target_to(target)
		else:
			change_target_to(globals.player)
	else:
		change_target_to(globals.player)

func change_target_to(target):
	if target == globals.player:
		move_target_index = -1
	if move_target != null and is_instance_valid(move_target):
		move_target.targeted_by_count -= 1
	move_target = target
	move_target.targeted_by_count += 1

func set_facing_direction():
	if move_target.global_position.x > self.global_position.x:
		$CharSprite.set_flip_h(false)
	else:
		$CharSprite.set_flip_h(true)