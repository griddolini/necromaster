extends Control

func _ready():
	globals.init()

func _on_Button2_pressed():
	get_tree().change_scene("res://stages/TestWorld.tscn")
	pass


func _on_Button_pressed():
	get_tree().change_scene("res://ui/HelpScreen.tscn")
	pass


func _on_Button4_pressed():
	get_tree().change_scene("res://ui/MainMenu.tscn")
	pass


func _on_Button3_pressed():
	get_tree().quit()
