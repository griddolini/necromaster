extends Node
const Powerline = preload("res://scenes/PowerLine.gd")
const Undead = preload("res://Undead/Undead.gd")
const Human = preload("res://Human/Human.gd")

var player = null
var ui = null
var undead_count = 0

var score_power = 0
var score_kills = 0
var score_time = 0

var player_power : int = 90
var player_maxPower : int = 100
var player_health : int = 10
var player_maxHealth : int = 10

var power_connections = []
var power_targets = []

var powerline_scene = preload("res://scenes/PowerLine.tscn")

func init():
	undead_count = 0
	score_power = 0
	score_kills = 0
	score_time = 0
	player_power = 90
	player_maxPower = 100
	player_health = 10
	player_maxHealth = 10
	power_connections = []
	power_targets = []

func _ready():
	pass

func _input(event):
	if event.is_action_pressed("fullscreen"):
    	OS.window_fullscreen = !OS.window_fullscreen
	if event.is_action_pressed("ui_cancel"):
		get_tree().change_scene("res://ui/MainMenu.tscn")

func draw_powerline(unit_1 : KinematicBody2D, unit_2 : KinematicBody2D):
	var line = powerline_scene.instance() as Powerline
	line.starting_unit = unit_1
	line.ending_unit = unit_2
	player.get_parent().call_deferred("add_child", line)
	power_connections.append(line)
	pass

func harvest_life():
	var num_units = get_harvest_count()
	var harvest_damage = 3 * num_units
	globals.player_health += num_units
	
	globals.player_power += harvest_damage * num_units * 3
	globals.score_power += harvest_damage * num_units * 3
	globals.ui.update_power()
	if num_units > 1:
		ui.show_harvest_combo(num_units)
	globals.ui.update_health()
	# We don't need to check for doubles here
	# since the targets wont accept the function twice in a row
	for target in power_targets:
		(target as Human).take_harvesting(harvest_damage)
	pass

func get_harvest_count() -> int:
	var duplicate_array = []
	var counter = 0
	for target in globals.power_targets:
		if duplicate_array.find(target) == -1:
			counter += 1
		duplicate_array.append(target)
	return counter