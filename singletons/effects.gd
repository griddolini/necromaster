extends Node
var hiteffect1_scene = preload("res://effects/HitEffect1.tscn")
var hiteffect2_scene = preload("res://effects/HitEffect2.tscn")
var hiteffect3_scene = preload("res://effects/HitEffect3.tscn")
var necrolight1_scene = preload("res://effects/NecroLight1.tscn")
var necrolight2_scene = preload("res://effects/NecroLight2.tscn")
var necrolight3_scene = preload("res://effects/NecroLight3.tscn")
var stuneffect_scene = preload("res://effects/StunEffect.tscn")
var bloodsplay = preload("res://effects/GroundSplat.tscn")

func create_hiteffect(global_pos : Vector2, index : int, parent = null):
	match index:
		1:
			var effect = hiteffect1_scene.instance()
			effect.set_global_position(global_pos)
			globals.player.get_parent().add_child(effect)
			create_hiteffect(global_pos, 5)
		2:
			var effect = necrolight1_scene.instance()
			effect.set_global_position(global_pos)
			if parent != null:
				parent.add_child(effect)
				effect.position = Vector2.ZERO
			else:
				globals.player.get_parent().add_child(effect)
		3:
			var effect = necrolight2_scene.instance()
			effect.set_global_position(global_pos)
			if parent != null:
				parent.add_child(effect)
				effect.position = Vector2.ZERO
			else:
				globals.player.get_parent().add_child(effect)
		4:
			var effect = necrolight3_scene.instance()
			effect.set_global_position(global_pos)
			if parent != null:
				parent.add_child(effect)
				effect.position = Vector2.ZERO
			else:
				globals.player.get_parent().add_child(effect)
		5:
			var effect = hiteffect3_scene.instance()
			effect.set_global_position(global_pos)
			globals.player.get_parent().add_child(effect)
			var blad = bloodsplay.instance()
			blad.set_global_position(global_pos)
			globals.player.get_parent().add_child(blad)
	pass

func create_hit_number(global_pos : Vector2, damage : int):
	var effect = hiteffect2_scene.instance()
	effect.text = str(damage)
	effect.set_global_position(global_pos)
	globals.player.get_parent().add_child(effect)

func create_stuneffect(global_pos : Vector2, duration : float, parent : Node = null):
	var effect = stuneffect_scene.instance()
	effect.set_global_position(global_pos)
	effect.duration = duration
	if parent == null:
		globals.player.get_parent().add_child(effect)
	else:
		parent.add_child(effect)
		effect.position = Vector2.ZERO
	