extends Node2D

func _ready():
	pass

func _on_Timer_timeout():
	self.queue_free()
	pass

func _on_ToggleTimer_timeout():
	$Sprite.visible = !$Sprite.visible
	pass
