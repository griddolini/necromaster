# 12 13 26 27

extends Node2D

var fly_height = 0
var fly_speed = 0
var fly_dir = Vector2.ZERO
var move_speed = 10

var landed = false

func _ready():
	fly_dir = Vector2.RIGHT.rotated(rand_range(-PI, PI))
	if fly_dir.x > 0:
		$Sprite.set_flip_h(true)
	fly_height = 1
	fly_speed = rand_range(2,8)
		
func _physics_process(delta):
	if not landed:
		$Sprite.rotate(0.1)
		$Sprite.position.y = -fly_height/3
		self.position += fly_dir.normalized() * move_speed * 0.05
		fly_height += fly_speed
		fly_speed -= 0.2
		if fly_height <= 0:
			fly_speed = 0
			fly_height = 0
			$Sprite.rotation = (rand_range(-PI, PI))
			landed = true
			$Sprite.modulate.a = 0.5