extends Node2D

export var do_shake = false
export var do_rotate = false

var text = ""
var duration = 0.0

func _ready():
	self.position.y -= 4
	if do_rotate:
		$Sprite.rotation = (rand_range(-PI, PI))
	if duration != 0.0:
		$Timer.start(duration)
	if text != "":
		$Sprite/Label.text = text
	
func _physics_process(delta):
	if text != "":
		self.position.y -= 0.2
	if do_shake:
		$Sprite.position = Vector2(rand_range(-1,1), rand_range(-1,1))

func _on_Timer_timeout():
	self.queue_free()
	pass
